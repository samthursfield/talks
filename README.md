Talks by Sam Thursfield
=======================

Here is a list of conference talks I've given, grouped by topic. Slides can be found in this repo as well.

End-to-end testing of operating systems
---------------------------------------

Most of these talks are about my off-and-on work to improve end-to-end testing in the world of desktop Linux.

  * [How to push your testing upstream](https://fosdem.org/2025/schedule/event/fosdem-2025-4805-how-to-push-your-testing-upstream/) at FOSDEM 2025
  * [Automated testing for mobile images using GNOME](https://fosdem.org/2025/schedule/event/fosdem-2025-5649-automated-testing-for-mobile-images-using-gnome/) at FOSDEM 2025
  * [The best testing tools we’ve ever had: an introduction to OpenQA for GNOME](https://www.youtube.com/watch?v=jIDk0frev7M&t=3h56m39s) at [GUADEC 2023](https://events.gnome.org/event/101/).
  * [Setting up OpenQA testing for GNOME](https://fosdem.org/2023/schedule/event/openqa_for_gnome/) at [FOSDEM 2023](https://fosdem.org/2023/).
  * [Move Fast and Break Everything: Testing major changes to a core
    component of GNOME](https://www.youtube.com/watch?v=dmRLBHoSGGQ)
    (GUADEC 2020)

Music
-----

The most important thing in life. Talks about
[Calliope](https://gitlab.com/samthursfield/calliope/), a framework for building
playlists and music recommendation tools.

  * [Simple, open, music recommendations with Python](https://www.fosdem.org/2023/schedule/event/python_music_recommendation/) at [FOSDEM 2023](https://www.fosdem.org/2023/)
  * [Simple. open music recommendations](https://www.youtube.com/watch?v=7APk4GqIlD0) at [EuroPython 2021](https://ep2021.europython.eu/).

Software build and integration tools
------------------------------------

The talks are mostly related to the [BuildStream integration tool](http://buildstream.build/).

  * [Integrating Software Stacks with BuildStream 2.0 and the Remote Execution API](https://www.youtube.com/watch?v=Mvi6pylo-8o) at [OSSEU 2022](https://osseu2022.sched.com/).
  * [Generating different base systems from the same inputs in Freedesktop SDK](https://www.youtube.com/watch?v=76Uw8QCBgGk)
    at [PackagingCon](https://packaging-con.org/) 2021
  * <a href="https://www.youtube.com/watch?v=6Yz8Y_QTcSM">Building Flatpak apps with BuildStream</a> (GUADEC 2018), a short talk about an unrecommended way of building apps
  * <a href="https://fosdem.org/2016/schedule/event/format_for_build_and_integration_instructions/">The Story of a Structured and Declarative Format for Build and Integration Instructions</a> (FOSDEM 2016), a long talk about a discontinued project called Baserock which was the forerunner of both <a href="https://buildstream.build/">BuildStream</a> and <a href="https://gitlab.com/freedesktop-sdk/freedesktop-sdk/">Freedesktop SDK</a>.
  * <a href="https://www.youtube.com/watch?v=qYGlMCk15hs">Introduction to Baserock</a> (EuroPython 2015), an earlier talk about Baserock.

Search in the desktop
---------------------

These talks relate to desktop search in the GNOME desktop, particularly the
[LocalSearch](https://gitlab.gnome.org/GNOME/localsearch/) project (formerly
Tracker).

  * [Tracker 3: The future is
    present](https://www.youtube.com/watch?v=QrH5WW0MD0c&t=216s) (with
    [Carlos Garnacho] at GUADEC 2020)
  * <a href="https://www.youtube.com/watch?v=YnY80QRBez4">Tracker 3.0</a> (with [Carlos Garnacho] at GUADEC 2019), a last-minute talk about the <a href="https://gnome.pages.gitlab.gnome.org/">Tracker search engine</a>.
  * <a href="https://www.youtube.com/watch?v=11wJTYxGAEo">Tracker: Introduction and Reflection</a> (GUADEC 2015), a long talk about an indexing and search tool for desktop users

[Carlos Garnacho]: https://blogs.gnome.org/carlosg/
